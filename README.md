#Overview

##This source code demonstrates how to display menu in circle.

```
<!DOCTYPE html>
<html>

<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <style>
        #parent {
            position: relative;
            top: 200px;
            left: 200px;
            list-style-type: none;
            margin: 0;
            padding: 0;
        }
        
        #parent div {
            position: absolute;
            -webkit-transition: all 2s linear;
            -moz-transition: all 2s linear;
            transition: all 2s linear;
            height: 80px;
            width: 80px;
            border-radius: 100px;
            background-image: url("https://cdn4.iconfinder.com/data/icons/e-commerce-and-business/104/New_Latest_Breaking-64.png");
            background-repeat: no-repeat;
            align-content: stretch;
        }
    </style>
</head>

<body>
    <div id="parent">
        <div> </div>
        <div> </div>
        <div> </div>
        <div> </div>
        <div> </div>
        <div> </div>
        <div> </div>
        <div> </div>
        <div> </div>
    </div>

    <script>
        var type = 1, //circle type - 1 whole, 0.5 half, 0.25 quarter
            radius = '12em', //distance from center
            start = -90, //shift start from 0
            $elements = $('#parent >div')
            , numberOfElements = (type === 1) ? $elements.length : $elements.length - 1
            , slice = 360 * type / numberOfElements;

        $elements.each(function (i) {
            var $self = $(this)
                , rotate = slice * i + start
                , rotateReverse = rotate * -1;

            $self.css({
                'transform': 'rotate(' + rotate + 'deg) translate(' + radius + ') rotate(' + rotateReverse + 'deg)'
            });
        });
    </script>

</body>

</html>


```

Adding or removing more <div> inside parent <div> you either increase or decrease the number menu size.

##Screen Shot
![Screen Shot](/circleMenu/circleMenu.png)
